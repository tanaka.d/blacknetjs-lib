import {SignMessage, VerifyMessage, Signature, Mnemonic, Address, Keypair, Decrypt, Encrypt,
    PublicKeyToAddress, PrivateToKeypair, ToKeypair} from './api';
import serialize from './serialize';
import JsonRPC from './jsonrpc';
import Option from './option';

class Blacknetjs {
    // static func
    public static Signature = Signature;
    public static VerifyMessage = VerifyMessage;
    public static SignMessage = SignMessage;
    public static Mnemonic = Mnemonic;
    public static Address = Address;
    public static Keypair = Keypair;
    public static Decrypt = Decrypt;
    public static Encrypt = Encrypt;
    public static PublicKeyToAddress = PublicKeyToAddress;
    public static PrivateToKeypair = PrivateToKeypair;
    public static ToKeypair = ToKeypair;
    // static class
    public static Serialize = serialize;
    public static JSONRPC = JsonRPC;
    public static Config = Option;
    // member
    public serialize: serialize;
    public jsonrpc: JsonRPC;
    public config: Option;
    constructor(config?: Option) {
        this.config = config || new Option();
        this.jsonrpc = new JsonRPC(this.config.endpoint);
        this.serialize = new serialize(this.jsonrpc);
    }
}

export default Blacknetjs;
export const Serialize = serialize;
export const JSONRPC = JsonRPC;
export const Config = Option;
