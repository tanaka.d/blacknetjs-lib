import {HexToPublicKey, PublicKey, PublicKeyToHex, PublicKeyToAccount} from '../utils';

const Uint64BE = require('int64-buffer').Uint64BE;

class WithdrawFromLease {
    public Withdraw: number; // 64
    public Amount: number; // 64
    public To: string;
    public Height: number; // 32
    constructor(withdraw: number, amount: number, to: string, height: number) {
        this.Withdraw = withdraw;
        this.Amount = amount;
        this.To = PublicKeyToHex(PublicKey(to));
        this.Height = height;
    }
    // Serialize
    public Serialize(): Uint8Array {
        const withdraw = new Uint64BE(this.Withdraw).buffer;
        const amount = new Uint64BE(this.Amount).buffer;
        const to = HexToPublicKey(this.To);
        const height = new Uint8Array(new Uint64BE(this.Height).buffer).subarray(4, 8);
        return Uint8Array.from([
            ...withdraw,
            ...amount,
            ...to,
            ...height
        ]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): WithdrawFromLease {
        const withdraw = new Uint64BE(arr.subarray(0, 8)).toNumber();
        const amount = new Uint64BE(arr.subarray(8, 16)).toNumber();
        const to = PublicKeyToAccount(arr.subarray(16, 48));
        const height = new Uint64BE(arr.subarray(48)).toNumber();
        return new WithdrawFromLease(withdraw, amount, to, height);
    }
}

export default WithdrawFromLease;
