import {HexToPublicKey, PublicKey, PublicKeyToHex, PublicKeyToAccount} from '../utils';

const Uint64BE = require('int64-buffer').Uint64BE;

class Lease {
    public Amount: number;
    public To: string;
    constructor(amount: number, to: string) {
        this.Amount = amount;
        this.To = PublicKeyToHex(PublicKey(to));
    }
    // Serialize
    public Serialize(): Uint8Array {
        const amount = new Uint64BE(this.Amount).buffer;
        const to = HexToPublicKey(this.To);
        return Uint8Array.from([...amount, ...to]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): Lease {
        const amount = new Uint64BE(arr.subarray(0, 8)).toNumber();
        const to = PublicKeyToAccount(arr.subarray(8, 40));
        return new Lease(amount, to);
    }
}

export default Lease;
