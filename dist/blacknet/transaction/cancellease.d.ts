declare class CancelLease {
    Amount: number;
    To: string;
    Height: number;
    constructor(amount: number, to: string, height: number);
    Serialize(): Uint8Array;
    static Derialize(arr: Uint8Array): CancelLease;
}
export default CancelLease;
