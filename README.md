[![npm](https://img.shields.io/npm/v/blacknetjs)](https://www.npmjs.com/package/blacknetjs)

# blacknetjs

## import
```
<script src="../dist/blacknet.js"></script>
// or
npm i blacknetjs

```

## Sign Message
```js

var testAccount = "blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036";

var message = "Blacknet is extensible proof-of-stake network.";

let signedMessage = blacknetjs.SignMessage(testMnemonic, message);

console.log(signedMessage);

```
## Verify Message
```js
let result = blacknetjs.VerifyMessage(testAccount, signedMessage, message);
console.log(result); // true
    
```
## Encrypt Message and Decrypt Message

```js
var fromKey = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin";
var fromAccount = blacknetjs.Address(fromKey);;
var toKey = "grape coconut enhance session educate round hole velvet liar harbor obey already";
var toAccount = blacknetjs.Address(fromKey);;
var secretMsg  = 'its a secret msg';

let encryptMsg = blacknetjs.Encrypt(fromKey, toAccount, secretMsg);
console.log(encryptMsg);

let decryptMsg = blacknetjs.Decrypt(toKey, fromAccount, encryptMsg);
console.log(decryptMsg);
```

## Generate Account

```js
let mnemonic = blacknetjs.Mnemonic();
let account = blacknetjs.Address(mnemonic);

```
## Local Sign then Send transaction

```js
// use blnmobiledaemon.blnscan.io
bln.serialize.transfer({ // lease or cancelLeease
    fee: 0.001*1e8,
    amount: 1*1e8,
    message: 'blacknet',
    from: testAccount2,
    to: testAccount2,
    encrypted: 1
}).then((res)=>{
    console.log(res.body);
})

```

# donate the developer
donate: blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj
